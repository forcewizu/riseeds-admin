import { expect, assert } from 'chai';
import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import Antd from 'ant-design-vue';
import TransactionsTable from '@/components/transactions/TransactionsTable.vue';
import transactions from '../../src/store/modules/transactions';

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Antd);

describe('Transactions', () => {
  let store

  store = new Vuex.Store({
    modules: {
      transactions,
    }
  })

  const wrapper = mount(TransactionsTable, { store, localVue });
  describe('TransactionsTable.vue', () => {
    it('has a "table" tag', () => {
      expect(wrapper.contains('table')).to.be.true;
    });
    it('"SortedInfo" is null', () => {
      assert.isNull(wrapper.vm.sortedInfo, 'SortedInfo is null')
    });
  });
});
