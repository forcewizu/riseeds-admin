# "Riseeds" admin dashboard

## Dependencies 
> * [Vue.JS](https://vuejs.org/)
> * [Vuex](https://vuex.vuejs.org/) (state control)
> * [Vue-router](https://router.vuejs.org/)
> * [Ant Design](https://vue.ant.design/) (UI)
> * [Axios](https://github.com/axios/axios) (http wrapper)
> * [Lodash]() (utility library)
> * [Moment]() (format dates)
> * [Numeral.js]() (format numbers)
> * [Vuelidate]() (forms validation)
> * [Qrcode.vue]() (QR Code generator)
> * [Logplease]() (logger)
> * Tests using [Mocha](https://mochajs.org/) & [Chai](https://www.chaijs.com/) 

## Project setup

### Install dependencies
```
npm install
```

### Compiles and hot-reloads for development with fake API ([JSON-server](https://github.com/typicode/json-server))
```
npm run dev
```

### Compiles and hot-reloads for development with production API
```
npm run dev:prod
```

### Run json-server by another terminal
```
npm run server
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
