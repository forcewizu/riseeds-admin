const path = require('path');

const ghpages = process.env.VUE_APP_MODE === 'ghpages';

const primaryColor = '#1890ff';

module.exports = {
  publicPath: ghpages
    ? '/riseeds-admin/'
    : '/',
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        path.resolve(__dirname, 'src/scss/core/*.scss'),
      ],
    },
  },
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          'primary-color': '#2cb17c',
          'link-color': '#008F56',
          'btn-primary-bg': primaryColor,
          'btn-default-color': primaryColor,
          'btn-default-border': primaryColor,
          'input-border-focus': '#66afe9',
          'input-hover-border-color': primaryColor
        },
        javascriptEnabled: true
      }
    }
  }
};
