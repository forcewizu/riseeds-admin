const config = {
  'prod': {
    API_ROOT: 'http://localhost:3000',
  },
  'fake': {
    API_ROOT: 'http://localhost:3000',
  },
};

const API_TYPE = process.env.VUE_APP_API;

const { API_ROOT } = config[API_TYPE];

const api = {
  transactions: `${API_ROOT}/transactions`,
  projects: `${API_ROOT}/projects`,
  profile: `${API_ROOT}/profile`,
  categories: `${API_ROOT}/categories`,
  users: `${API_ROOT}/users`,
}

export default api;