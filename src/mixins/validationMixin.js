import { required, sameAs, minLength } from 'vuelidate/lib/validators';

import validation from '../services/validation';
import http from '../services/httpService';

const validationMixin = {
  data() {
    return {
      isBirthError: false,
    }
  },
  validations: {
    formData: {
      email: {
        required,
        email: validation.email,
      },
      name: {
        required,
        minLength: minLength(2),
      },
    },
    currentPassword: {
      password: validation.password,
      async isYourPassword(value) {
        return Boolean(await http.checkPassword(value))
      }
    },
    newPassword: {
      password: validation.password,
      match: sameAs('currentPassword'),
    },
  },
  methods: {
    onBirthError(e) {
      this.isBirthError = !!e;
    },
    dateErrorMessage(error) {
      if (error) {
        return `Date is incorrect`;
      }
      return '';
    },
    passwordMatch() {
      return validation.password(this.$v.currentPassword) && this.$v.newPassword === this.$v.currentPassword;
    }
  },
  computed: {
    emailStatus() {
      if (this.$v.formData.email.$error) {
        return 'warning';
      }
      return '';
    },
    emailErrorMessage() {
      if (!this.$v.formData.email.required) {
        return `It's a required field`;
      } else if (!this.$v.formData.email.email) {
        return `Email is incorrect`;
      }
      return '';
    },
    nameStatus() {
      if (this.$v.formData.name.$error) {
        return 'warning';
      }
      return '';
    },
    nameErrorMessage() {
      if (!this.$v.formData.name.required) {
        return `It's a required field`;
      } else if (!this.$v.formData.name.minLength) {
        return `Name is incorrect`;
      }
      return '';
    },
    currentPasswordErrorMessage() {
      if (this.currentPassword.length && this.$v.currentPassword.$error && !this.$v.currentPassword.password) {
        return `Password is incorrect`;
      }
      return '';
    },
    newPasswordErrorMessage() {
      if (this.newPassword.length && this.$v.newPassword.$error) {
        if (!this.$v.newPassword.password) {
          return `Password is incorrect`;
        } else if (!this.$v.newPassword.match) {
          return `Password doesn't match`;
        }
      }
      return '';
    },
    currentPasswordStatus() {
      if (this.currentPassword.length && this.$v.currentPassword.$error) {
        return `warning`;
      }
      return '';
    },
    newPasswordStatus() {
      if (this.newPassword.length && this.$v.newPassword.$error) {
        return `warning`;
      }
      return '';
    },
    isChangePasswordDisables() {
      return this.$v.newPassword.$invalid || this.$v.currentPassword.$invalid;
    }
  },
};

export default validationMixin;
