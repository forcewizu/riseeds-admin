const countryList = require('country-list');
import { mapState, mapMutations, mapActions } from 'vuex';
import {
  UPDATE_PROJECT_VALUE,
  UPDATE_PROJECT_ACCOMPLISHMENT,
  ADD_PROJECT_ACCOMPLISHMENT,
  REMOVE_PROJECT_LOGO,
  REMOVE_PROJECT_IMAGE,
  ADD_PROJECT_MEMBER,
  UPDATE_PROJECT_MEMBER,
} from '../constants';

export default {
  data() {
    return {
      countryList,
      logoLoading: false,
      imageLoading: false,
      editeMember: null,
    };
  },
  computed: {
    ...mapState({
      formData: state => state.project.formData,
      user: state => state.users.user,
    }),
    name: {
      get() {
        return this.formData.name;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['name', v]);
      },
    },
    country: {
      get() {
        return this.formData.country;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['country', v]);
      },
    },
    raise: {
      get() {
        return `$${this.$numeral(this.formData.raise).format('0,0.[00]')}`;
      },
      set(v) {
        const num = this.$numeral(v).value();
        if (!Number.isNaN(Number(num))) {
          this[UPDATE_PROJECT_VALUE](['raise', num]);
        }
      },
    },
    customers: {
      get() {
        return `${this.$numeral(this.formData.customers).format('0,0')}`;
      },
      set(v) {
        const num = this.$numeral(v).value();
        if (!Number.isNaN(Number(num))) {
          this[UPDATE_PROJECT_VALUE](['customers', num]);
        }
      },
    },
    capital: {
      get() {
        return `$${this.$numeral(this.formData.capital).format('0,0.[00]')}`;
      },
      set(v) {
        const num = this.$numeral(v).value();
        if (!Number.isNaN(Number(num))) {
          this[UPDATE_PROJECT_VALUE](['capital', num]);
        }
      },
    },
    isRaised: {
      get() {
        return this.formData.is_raised;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['is_raised', v]);
      },
    },
    isLaunched: {
      get() {
        return this.formData.is_launched;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['is_launched', v]);
      },
    },
    receive: {
      get() {
        return this.formData.receive;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['receive', v]);
      },
    },
    compaign: {
      get() {
        return this.formData.compaign;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['compaign', v]);
      },
    },
    currentValuation: {
      get() {
        return `$${this.$numeral(this.formData.current_valuation).format(
          '0,0.[00]',
        )}`;
      },
      set(v) {
        const num = this.$numeral(v).value();
        if (!Number.isNaN(Number(num))) {
          this[UPDATE_PROJECT_VALUE](['current_valuation', num]);
        }
      },
    },
    maxAmount: {
      get() {
        return this.formData.max_amount;
      },
      set(v) {
        if (!Number.isNaN(Number(v))) {
          this[UPDATE_PROJECT_VALUE](['max_amount', Number(v)]);
        }
      },
    },
    website: {
      get() {
        return this.formData.website;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['website', v]);
      },
    },
    sector: {
      get() {
        return this.formData.sector;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['sector', v]);
      },
    },
    bussiness_description: {
      get() {
        return this.formData.bussiness_description;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['bussiness_description', v]);
      },
    },
    title: {
      get() {
        return this.formData.title;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['title', v]);
      },
    },
    phone: {
      get() {
        return this.formData.phone;
      },
      set(v) {
        this[UPDATE_PROJECT_VALUE](['phone', v]);
      },
    },
  },
  methods: {
    ...mapMutations('project', [
      UPDATE_PROJECT_VALUE,
      UPDATE_PROJECT_ACCOMPLISHMENT,
      ADD_PROJECT_ACCOMPLISHMENT,
      ADD_PROJECT_MEMBER,
      UPDATE_PROJECT_MEMBER,
    ]),
    ...mapActions('project', [
      REMOVE_PROJECT_LOGO,
      REMOVE_PROJECT_IMAGE,
    ]),
    filterOption(input, option) {
      return (
        option.componentOptions.children[0].text
          .toUpperCase()
          .indexOf(input.toUpperCase()) >= 0
      );
    },
    handleLogoChange(info) {
      if (info.file.status === 'uploading') {
        this.logoLoading = true;
        return;
      }
      if (info.file.status === 'done') {
        this.$utils.getBase64(info.file.originFileObj, imageUrl => {
          this[UPDATE_PROJECT_VALUE](['logo_url', imageUrl]);
          this.logoLoading = false;
        });
      }
    },
    handleImageChange(info) {
      if (info.file.status === 'uploading') {
        this.imageLoading = true;
        return;
      }
      if (info.file.status === 'done') {
        this.$utils.getBase64(info.file.originFileObj, imageUrl => {
          this[UPDATE_PROJECT_VALUE](['image_url', imageUrl]);
          this.imageLoading = false;
        });
      }
    },
  },
};