import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import _ from 'lodash';
import numeral from 'numeral';
import Antd, { Modal, message } from 'ant-design-vue';
import 'ant-design-vue/dist/antd.less';
import './scss/style.scss';
import VueMoment from 'vue-moment';
import vueNumeralFilterInstaller from 'vue-numeral-filter';
import utils from './utils/utils';
import BioComponents from './plugins/components';
import Vuelidate from 'vuelidate';
import VueTelInput from 'vue-tel-input';

new BioComponents();

Vue.use(VueTelInput);
Vue.use(Antd);
Vue.use(vueNumeralFilterInstaller);
Vue.use(VueMoment);
Vue.use(Vuelidate);

Vue.prototype.$lodash = _;
Vue.prototype.$numeral = numeral;
Vue.prototype.$utils = utils;
Vue.prototype.$message = message;
Vue.prototype.$confirm = Modal.confirm;

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
