import axios from 'axios';
import api from '../api';
import logger from './logger';

if (process.env.VUE_APP_API === 'fake') {
  axios.interceptors.response.use(
    response => {
      return new Promise(resolve => {
        setTimeout(() => resolve(response), 1500);
      });
    },
    error => {
      return Promise.reject(error);
    },
  );
}

class HttpService {
  _fetchData = async (type, api, body = '') => {
    try {
      const { data } = await axios[type](api, body);
      logger.info(api, data, true);
      return data;
    } catch (error) {
      logger.apiError(error.response);
      throw error;
    }
  };
  getProfile = async () => {
    return this._fetchData('get', api.profile);
  };
  updateProfile = async formData => {
    return this._fetchData('patch', api.profile, formData);
  };
  getTransactions = async id => {
    const body = id ? { params: { id } } : '';
    return this._fetchData('get', api.transactions, body);
  };
  getProjects = async id => {
    const body = id ? { params: { id } } : '';
    return this._fetchData('get', api.projects, body);
  };
  _updateProjectsStatus = async (id, status) => {
    return this._fetchData('patch', `${api.projects}/${id}`, {
      status,
    });
  };
  rejectProjectStatus = async id => {
    return this._updateProjectsStatus(id, 'rejected');
  };
  approveProjectStatus = async id => {
    return this._updateProjectsStatus(id, 'approved');
  };
  toggleProjectFavorite = async (id, favorite) => {
    return this._fetchData('patch', `${api.projects}/${id}`, {
      favorite,
    });
  };
  getCategories = async () => {
    return this._fetchData('get', api.categories);
  };
  addCategory = async body => {
    return this._fetchData('post', api.categories, body);
  };
  removeCategory = async id => {
    return this._fetchData('delete', `${api.categories}/${id}`);
  };
  updateCategory = async body => {
    return this._fetchData('patch', `${api.categories}/${body.id}`, body);
  };
  checkPassword = async body => {
    // TODO: change this request
    return Promise.resolve(body);
  };
  changePassword = async body => {
    // TODO: change this request
    return new Promise(resolve => {
      setTimeout(() => resolve(body), 1500);
    });
  };
  getQRCode = async id => {
    // TODO: change this request
    return new Promise(resolve => {
      const code =
        Math.random()
          .toString(36)
          .substring(4) + id;
      setTimeout(() => resolve(code), 1500);
    });
  };
  sendAuthCode = async () => {
    // TODO: change this request
    return this.updateProfile({ isTwoFactor: true });
  };
  disableTwoStep = async () => {
    // TODO: change this request
    return this.updateProfile({ isTwoFactor: false });
  };
  getUsers = async () => {
    return this._fetchData('get', api.users);
  };
  getUser = async id => {
    return this._fetchData('get', `${api.users}/${id}`);
  };
  updateUser = async (payload) => {
    return this._fetchData('patch', `${api.users}/${payload.id}`, {...payload});
  };
  removeUser = async (id) => {
    return this._fetchData('delete', `${api.users}/${id}`);
  };
  updateProject = async (payload) => {
    return this._fetchData('patch', `${api.projects}/${payload.id}`, {...payload});
  };
}

export default new HttpService();
