import * as Logger from 'logplease';

const ENV_MODE = process.env.NODE_ENV === 'development';

const toLogger = (name = '', msg, dev, type) => {
  if (dev && !ENV_MODE) return;
  const newLogger = Logger.create(name);
  newLogger[type](msg);
};

const logger = {
  debug: (name = '', msg, dev) => {
    toLogger(name, msg, dev, 'debug');
  },
  info: (name = '', msg, dev) => {
    toLogger(name, msg, dev, 'info');
  },
  error: (name = '', msg, dev) => {
    toLogger(name, msg, dev, 'error');
  },
  apiError: ({ status = '', config: { url = '' }, data = '' }, msg, dev) => {
    if (dev && !ENV_MODE) return;
    const newLogger = Logger.create(`${status} ${url}`);
    newLogger.error(msg?msg:data);
  },
  warn: (name = '', msg, dev) => {
    toLogger(name, msg, dev, 'warn');
  },
  log: (name = '', msg, dev) => {
    toLogger(name, msg, dev, 'log');
  },
}

export default logger;
