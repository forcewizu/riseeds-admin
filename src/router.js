import Vue from 'vue';
import Router from 'vue-router';

const TransactionsPage = () => import(/* webpackChunkName: "transactions" */ './views/TransactionsPage.vue');
const ProjectsPage = () => import(/* webpackChunkName: "projects" */ './views/ProjectsPage.vue');
const SettingsPage = () => import(/* webpackChunkName: "settings" */ './views/SettingsPage.vue');
const UsersPage = () => import(/* webpackChunkName: "users" */ './views/UsersPage.vue');
const UserPage = () => import(/* webpackChunkName: "user" */ './views/UserPage.vue');
const Project = () => import(/* webpackChunkName: "project" */ './views/Project.vue');
const AppProjectsRequests = () => import(/* webpackChunkName: "AppProjectsRequests" */ './components/projects/request/AppProjectsRequests.vue');
const AppProjectsActive = () => import(/* webpackChunkName: "AppProjectsActive" */ './components/projects/active/AppProjectsActive.vue');
const AppProjectsSettings = () => import(/* webpackChunkName: "AppProjectsSettings" */ './components/projects/settings/AppProjectsSettings.vue');
const AppSettingsGeneral = () => import(/* webpackChunkName: "AppSettingsGeneral" */ './components/settings/AppSettingsGeneral.vue');
const AppSettingsSecurity = () => import(/* webpackChunkName: "AppSettingsSecurity" */ './components/settings/AppSettingsSecurity.vue');

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      name: 'home',
      redirect: '/',
    },
    {
      path: '/',
      name: 'home',
      component: TransactionsPage,
    },
    {
      path: '/transactions',
      name: 'transactions',
      component: TransactionsPage,
    },
    {
      path: '/projects',
      component: ProjectsPage,
      children: [
        {
          path: '',
          name: 'projects',
          component: AppProjectsRequests,
        },
        {
          path: 'requests',
          name: 'projects-requests',
          component: AppProjectsRequests,
        },
        {
          path: 'active',
          name: 'projects-active',
          component: AppProjectsActive,
        },
        {
          path: 'settings',
          name: 'projects-settings',
          component: AppProjectsSettings,
        }
      ]
    },
    {
      path: '/settings',
      component: SettingsPage,
      children: [
        {
          path: '',
          name: 'settings',
          component: AppSettingsGeneral,
        },
        {
          path: 'security',
          name: 'settings-security',
          component: AppSettingsSecurity,
        }
      ]
    },
    {
      path: '/users',
      name: 'users',
      component: UsersPage,
    },
    {
      path: '/user/:id',
      name: 'user',
      component: UserPage,
    },
    {
      path: '/project/:id',
      name: 'project',
      component: Project,
    },
  ]
})
