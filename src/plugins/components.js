import Vue from 'vue';

import BioTransition from '../components/common/animate/BioTransition.vue';
import BioLoading from '../components/common/animate/BioLoading.vue';
import BioStatus from '../components/common/BioStatus.vue';
import BioCardNumber from '../components/common/BioCardNumber.vue';
import BioHeader from '../components/common/BioHeader.vue';
import BioDatePicker from '../components/common/form/BioDatePicker.vue';
import BioProjectCard from '../components/common/BioProjectCard.vue';
import BioHasChanges from '../components/common/form/BioHasChanges.vue';

export default class BioComponents {

  constructor() {
    Vue.component('BioTransition', BioTransition);
    Vue.component('BioStatus', BioStatus);
    Vue.component('BioCardNumber', BioCardNumber);
    Vue.component('BioHeader', BioHeader);
    Vue.component('BioDatePicker', BioDatePicker);
    Vue.component('BioLoading', BioLoading);
    Vue.component('BioProjectCard', BioProjectCard);
    Vue.component('BioHasChanges', BioHasChanges);
  }
}