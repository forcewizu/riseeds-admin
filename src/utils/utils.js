import moment from 'moment';

class Utils {

  truncate = ({  str, length = 100, sufix = '...'}) => {
    let maxLength = length;
    const result = (max) => {
      if (Number(str.length) <= Number(max)) return str;
      return `${str.substring(0, max)}${sufix}`;
    }
    return result(maxLength);
  }

  onSearch = (data, query) => {
    return data.filter((item) => {
      return item.toUpperCase().indexOf(query.toUpperCase()) >= 0;
    })
  }

  getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }

  lightenDarkenColor = (col, amt) => {
  
    let usePound = false;

    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }

    const num = parseInt(col,16);

    let r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if  (r < 0) r = 0;

    let b = ((num >> 8) & 0x00FF) + amt;

    if (b > 255) b = 255;
    else if  (b < 0) b = 0;

    let g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return ((usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16)).toUpperCase();

  }

  sortByString = (a, b, param) => {
    return (a[param] > b[param]) - (a[param] < b[param]);
  }

  sortByDate = (a, b, param) => {
    let toNumber = (x) => Number(moment(x[param]).valueOf());
    return toNumber(a) - toNumber(b);
  }

  sortByNumber = (a, b, param) => {
    return Number(a[param]) - Number(b[param]);
  }

  sortByBoolean = (a, b, param) => {
    return (a[param] === b[param])? 0 : a[param]? -1 : 1;
  }
}

export default new Utils();
