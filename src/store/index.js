import Vue from 'vue';
import Vuex from 'vuex';

import {
  LOADING,
} from '../constants';

import transactions from './modules/transactions';
import projects from './modules/projects';
import profile from './modules/profile';
import users from './modules/users';
import project from './modules/project';

Vue.use(Vuex);

const loadingPlugin = (store) => {
  store.subscribe(({ type, payload }, state) => {
    const re = /LOADING/g;
    const re2 = /UPLOADING/g;
    if (re.test(type) && state.loading !== payload && !re2.test(type)) {
      store.commit(LOADING, payload);
    }
  });
};

export default new Vuex.Store({
  state: {
    loading: false,
  },
  mutations: {
    [LOADING](state, status = true) {
      state.loading = status;
    },
  },
  actions: {},
  modules: {
    transactions,
    projects,
    profile,
    users,
    project,
  },
  plugins: [loadingPlugin]
});
