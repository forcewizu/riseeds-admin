import http from '../../services/httpService';
import _ from 'lodash';
import {
  LOADING,
  GET_USERS,
  UPDATE_USERS,
  REMOVE_USER,
  GET_USER,
  UPDATE_USER,
  UPDATE_USER_OPTION,
  UPLOADING,
  CANCEL_CHANGES,
} from '../../constants';

import { message } from 'ant-design-vue';

const user = {
  id: '',
  name: '',
  email: '',
  signup_date: '',
  avatar: '',
  email_confirmed: false,
  kyc: false,
  balance: 0,
  role: '',
  date_birth: '',
  address: '',
  country: '',
  postcode: '',
  flat: '',
  house: '',
  address_line_1: '',
  address_line_2: '',
  city: '',
};

const state = {
  users: [],
  user: {
    ...user,
  },
  formData: {
    ...user,
  },
  loading: false,
  uploading: false,
};

const mutations = {
  [LOADING](state, status = true) {
    state.loading = status;
  },
  [UPLOADING](state, status = true) {
    state.uploading = status;
  },
  [UPDATE_USERS](state, payload) {
    state.users = [...payload];
  },
  [UPDATE_USER](state, payload) {
    state.user = Object.assign({}, state.user, payload);
    state.formData = Object.assign({}, state.formData, payload);
  },
  [UPDATE_USER_OPTION](state, [label, value]) {
    state.formData[label] = value;
  },
  [CANCEL_CHANGES](state) {
    state.formData = Object.assign({}, state.formData, state.user);
  },
};

const getters = {
  isAdmin: (state) => state.user.role.toLowerCase() === 'admin',
  isInvestor: (state) => state.user.role.toLowerCase() === 'investor',
  isEnt: (state) => state.user.role.toLowerCase() === 'entrepreneur',
  hasChanges: (state) => {
    return !_.isEqual(state.user, state.formData);
  },
};

const actions = {
  async [GET_USERS]({ commit }) {
    commit(LOADING, true);
    try {
      const payload = await http.getUsers();
      commit(UPDATE_USERS, payload);
    } catch (error) {
      message.warn('Users not loaded');
    }
    commit(LOADING, false);
    return;
  },
  async [GET_USER]({ commit }, id) {
    commit(LOADING, true);
    try {
      const payload = await http.getUser(id);
      commit(UPDATE_USER, payload);
      commit(LOADING, false);
      return;
    } catch (error) {
      commit(LOADING, false);
      commit(UPDATE_USER, user);
      message.warn('User not loaded');
      throw error;
    }
  },
  async [REMOVE_USER]({ commit, dispatch }, id) {
    commit(LOADING, true);
    try {
      await http.removeUser(id);
      await dispatch(GET_USERS);
      message.info('User deleted successfully');
    } catch (error) {
      message.warn('User not deleted');
    }
    commit(LOADING, false);
    return;
  },
  async [UPDATE_USER]({ commit, state }) {
    try {
      commit(UPLOADING);
      await http.updateUser(state.formData);
      message.success('User profile updated successfully');
      commit(UPDATE_USER, state.formData);
    } catch (error) {
      message.warn('User profile not updated');
    }
    commit(UPLOADING, false);
    return;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
