import http from '../../services/httpService';
import {
  UPDATE_TRANSACTIONS,
  GET_TRANSACTIONS,
  LOADING,
} from '../../constants';

import { message } from 'ant-design-vue';

const transaction = {
  id: '',
  key: '',
  user_id: '',
  project_name: "",
  project_id: '',
  name: "",
  date: "",
  card: '',
  amount: '',
  status: ""
};

const state = {
  transactions: [],
  loading: false,
  transaction: {...transaction},
};

const mutations = {
  [UPDATE_TRANSACTIONS](state, data) {
    state.transactions = [...data];
  },
  [LOADING](state, status = true) {
    state.loading = status;
  },
};

// const getters = {};

const actions = {
  async [GET_TRANSACTIONS]({ commit }) {
    commit(LOADING, true);
    try {
      const data = await http.getTransactions();
      commit(UPDATE_TRANSACTIONS, data);
      commit(LOADING, false);
      return;
    } catch (error) {
      commit(LOADING, false);
      message.warn("Transactions not loaded");
      throw error;
    }
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
