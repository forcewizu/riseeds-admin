import http from '../../services/httpService';
import _ from 'lodash';
import {
  UPDATE_PROFILE,
  GET_PROFILE,
  LOADING,
  UPDATE_PROFILE_OPTION,
  CANCEL_CHANGES,
  CHANGE_PASSWORD,
  GET_QRCODE,
  UPDATE_QRCODE,
  UPDATE_AUTHCODE,
  SEND_AUTHCODE,
  DISABLE_TWO_STEP,
} from '../../constants';

import { message } from 'ant-design-vue';

const state = {
  profile: {
    id: null,
    name: '',
    email: '',
    date_birth: '',
    email_confirmed: false,
    isTwoFactor: false,
  },
  formData: {
    id: null,
    name: '',
    email: '',
    date_birth: '',
    email_confirmed: false,
    isTwoFactor: false,
  },
  loading: false,
  qrcode: '',
};

const mutations = {
  [UPDATE_PROFILE](state, payload) {
    state.profile = Object.assign({}, state.profile, payload);
    state.formData = Object.assign({}, state.formData, payload);
  },
  [UPDATE_PROFILE_OPTION](state, { name, value }) {
    state.formData[name] = value;
  },
  [LOADING](state, status = true) {
    state.loading = status;
  },
  [CANCEL_CHANGES](state) {
    state.formData = Object.assign({}, state.formData, state.profile);
  },
  [UPDATE_QRCODE](state, payload) {
    state.qrcode = payload;
  },
  [UPDATE_AUTHCODE](state, payload) {
    state.authCode = payload;
  },
};

const getters = {
  hasChanges: state => {
    return !_.isEqual(state.profile, state.formData);
  },
  isTwoFactor: state => state.profile.isTwoFactor,
};

const actions = {
  async [GET_PROFILE]({ commit }) {
    commit(LOADING, true);
    try {
      const data = await http.getProfile();
      commit(UPDATE_PROFILE, data);
      commit(LOADING, false);
      return;
    } catch (error) {
      commit(LOADING, false);
      message.warn('Profile not loaded');
      return;
    }
  },
  async [UPDATE_PROFILE]({ state, dispatch }) {
    try {
      await http.updateProfile(state.formData);
      await dispatch(GET_PROFILE);
      message.success('Profile updated successfully');
      return;
    } catch (error) {
      message.warn('Profile not loaded');
      return;
    }
  },
  async [CHANGE_PASSWORD]({ commit }, password) {
    commit(LOADING, true);
    try {
      await http.changePassword({ password });
      commit(LOADING, false);
      message.success('Password changed successfully');
      return;
    } catch (error) {
      message.warn('Password not changed');
      commit(LOADING, false);
      return;
    }
  },
  async [GET_QRCODE]({ state, commit }) {
    const { id } = state.profile;
    try {
      const code = await http.getQRCode(id);
      commit(UPDATE_QRCODE, code);
      return;
    } catch (error) {
      message.warn('QRCode not loaded');
      return;
    }
  },
  async [SEND_AUTHCODE]({ dispatch }, authCode) {
    try {
      await http.sendAuthCode(authCode);
      await dispatch(GET_PROFILE);
      message.success('Two factor authentication activated');
      return;
    } catch (error) {
      message.warn('Two factor authentication not activated');
      return;
    }
  },
  async [DISABLE_TWO_STEP]({ dispatch }) {
    try {
      await http.disableTwoStep();
      await dispatch(GET_PROFILE);
      message.info('Two factor authentication disabled');
      return;
    } catch (error) {
      message.warn('Two factor authentication not disabled');
      return;
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
