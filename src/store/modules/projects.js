import http from '../../services/httpService';
import _ from 'lodash';

import { message } from 'ant-design-vue';

import {
  GET_PROJECTS,
  LOADING,
  UPDATE_PROJECTS,
  APPROVE_PROJECT,
  REJECT_PROJECT,
  TOGGLE_PROJECT_FAVORITE,
  UPDATE_CATEGORIES,
  ADD_CATEGORY,
  REMOVE_CATEGORY,
  GET_CATEGORIES,
  UPDATE_CATEGORY,
  UPDATE_CATEGORY_OF_SNAPSHOT,
  ADD_CATEGORY_TO_SNAPSHOT,
} from '../../constants';

const state = {
  categoriesSnapshot: [],
  projects: [],
  categories: [],
  loading: false,
};

const mutations = {
  [UPDATE_PROJECTS](state, payload) {
    state.projects = [...payload];
  },
  [LOADING](state, status = true) {
    state.loading = status;
  },
  [UPDATE_CATEGORIES](state, payload) {
    state.categoriesSnapshot = [...payload];
    state.categories = [...payload];
  },
  [UPDATE_CATEGORY](state, {id, value}) {
    const idx = _.findIndex(state.categories, { id });
    state.categories.splice(idx, 1, {id, name: value});
  },
  [UPDATE_CATEGORY_OF_SNAPSHOT](state, payload) {
    const idx = _.findIndex(state.categoriesSnapshot, { id: payload.id });
    state.categoriesSnapshot.splice(idx, 1, payload);
  },
  [ADD_CATEGORY](state) {
    state.categories.push({
      id: Math.floor(Math.random() * 10000000) + 1,
      name: '',
    });
  },
  [ADD_CATEGORY_TO_SNAPSHOT](state, payload) {
    state.categoriesSnapshot.push(payload);
  },
  [REMOVE_CATEGORY](state, id) {
    const idx = _.findIndex(state.categories, { id });
    state.categories.splice(idx, 1);
  },
};

const getters = {
  isNewCategory: (state) => (hasId) => state.categoriesSnapshot.every(({id}) => Number(id) !== Number(hasId)),
  isChangedCategory: (state) => (newItem) => {
    const oldItem = state.categoriesSnapshot.find(({ id }) => id === newItem.id);
    if (oldItem) {
      return !_.isEqual(newItem, oldItem);
    }
    return false;
  },
  userProjects: (state) => (creator_id) => {
    return _.filter(state.projects, { creator_id });
  },
  activeProjects: (state) => {
    return state.projects.filter(({ status }) => status.toLowerCase() === 'approved');
  },
}

const actions = {
  async [ADD_CATEGORY]({ commit, dispatch }, formData) {
    commit(LOADING, true);
    try {
      await http.addCategory(formData);
      commit(LOADING, false);
      commit(ADD_CATEGORY_TO_SNAPSHOT, formData);
      message.success("Category added successfully");
    } catch (error) {
      message.warn("Category not added");
      dispatch(GET_CATEGORIES);
      commit(LOADING, false);
    }
    return;
  },
  async [REMOVE_CATEGORY]({ state, commit, dispatch }, hasId) {
    commit(LOADING, true);
    try {
      if (state.categoriesSnapshot.every(({id}) => id !== hasId)) {
        commit(REMOVE_CATEGORY, hasId);
        commit(LOADING, false);
        message.info("Category removed successfully");
        return;
      }
      commit(REMOVE_CATEGORY, hasId);
      await http.removeCategory(hasId);
      commit(LOADING, false);
      message.info("Category removed successfully");
    } catch (error) {
      message.warn("Category not removed");
      dispatch(GET_CATEGORIES);
      commit(LOADING, false);
    }
    return;
  },
  async [UPDATE_CATEGORY]({ commit, dispatch }, formData) {
    commit(LOADING, true);
    try {
      await http.updateCategory(formData);
      commit(LOADING, false);
      message.success("Category updated successfully");
      commit(UPDATE_CATEGORY_OF_SNAPSHOT, formData);
    } catch (error) {
      message.warn("Category not updated");
      dispatch(GET_CATEGORIES);
    }
    return;
  },
  async [GET_CATEGORIES]({ commit }) {
    commit(LOADING, true);
    try {
      const data = await http.getCategories();
      commit(UPDATE_CATEGORIES, data);
      commit(LOADING, false);
    } catch (error) {
      message.warn("Categories not loaded");
      commit(LOADING, false);
    }
    return;
  },
  async [GET_PROJECTS]({ commit }) {
    commit(LOADING, true);
    try {
      const data = await http.getProjects();
      commit(UPDATE_PROJECTS, data);
      commit(LOADING, false);
    } catch (error) {
      message.warn("Projects not loaded");
      commit(LOADING, false);
    }
    return;
  },
  async [APPROVE_PROJECT]({ dispatch }, id) {
    try {
      await http.approveProjectStatus(id);
      await dispatch(GET_PROJECTS);
      message.success('Project approved successfully');
    } catch (error) {
      message.warn('Project not approved');
    }
    return;
  },
  async [REJECT_PROJECT]({ dispatch }, id) {
    try {
      await http.rejectProjectStatus(id);
      await dispatch(GET_PROJECTS);
      message.success('Project rejected successfully');
    } catch (error) {
      message.warn('Project not rejected');
    }
    return;
  },
  
  async [TOGGLE_PROJECT_FAVORITE]({ state, dispatch }, favoriteId) {
    const { favorite } = state.projects.find(({ id }) => Number(id) === Number(favoriteId));
    try {
      await http.toggleProjectFavorite(favoriteId, !favorite);
      await dispatch(GET_PROJECTS);
    } catch (error) {
      message.warn("Favorite project not selected");
    }
    return;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
