import http from '../../services/httpService';
import _ from 'lodash';

import { message } from 'ant-design-vue';

import {
  LOADING,
  GET_PROJECT,
  UPDATE_PROJECT,
  UPDATE_PROJECT_VALUE,
  UPDATE_PROJECT_ACCOMPLISHMENT,
  ADD_PROJECT_ACCOMPLISHMENT,
  REMOVE_PROJECT_LOGO,
  REMOVE_PROJECT_IMAGE,
  ADD_PROJECT_MEMBER,
  UPDATE_PROJECT_MEMBER,
  CANCEL_CHANGES,
  UPLOADING,
} from '../../constants';

const project = {
  id: '',
  key: '',
  logo: '',
  creator: '',
  creator_id: '',
  image: '',
  name: '',
  description: '',
  invested: '',
  request_date: '',
  amount_status: '',
  amount: '',
  investors: '',
  equity: '',
  valuation: '',
  favorite: null,
  status: null,
  country: '',
  raise: '',
  customers: '',
  capital: '',
  is_raised: null,
  is_launched: null,
  receive: null,
  compaign: null,
  current_valuation: 0,
  max_amount: 0,
  website: '',
  sector: '',
  bussiness_description: '',
  accomplishments: [{ body: 'asdasdad'}],
  logo_url: '',
  image_url: '',
  title: '',
  phone: '',
  documents: [{
    uid: '-1',
    name: 'xxx.png',
    status: 'done',
    url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
    thumbUrl: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
  }, {
    uid: '-2',
    name: 'yyy.png',
    status: 'done',
    url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
    thumbUrl: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
  }],
  team: [{id: 1, name: 'Petro Poroshenko'}],
};

const state = {
  project: { ...project },
  formData: { ...project },
  loading: false,
  uploading: false,
};

const mutations = {
  [LOADING](state, payload = true) {
    state.loading = payload;
  },
  [UPLOADING](state, status = true) {
    state.uploading = status;
  },
  [UPDATE_PROJECT](state, payload) {
    state.project = Object.assign({}, state.project, payload);
    state.formData = Object.assign({}, state.formData, payload);
  },
  [UPDATE_PROJECT_ACCOMPLISHMENT](state, [idx, value]) {
    state.formData.accomplishments.splice(idx, 1, {body: value});
  },
  [UPDATE_PROJECT_MEMBER](state, [id, value]) {
    const idx = state.formData.team.findIndex(({ id: memberId }) => memberId === id);
    state.formData.team.splice(idx, 1, {id, name: value});
  },
  [UPDATE_PROJECT_VALUE](state, [label, value]) {
    state.formData[label] = value;
  },
  [ADD_PROJECT_ACCOMPLISHMENT](state) {
    state.formData.accomplishments.push({body: ''});
  },
  [REMOVE_PROJECT_LOGO](state) {
    state.logo_url = '';
  },
  [REMOVE_PROJECT_IMAGE](state) {
    state.image_url = '';
  },
  [ADD_PROJECT_MEMBER](state) {
    state.formData.team.push({id: Date.now(), name: ''});
  },
  [CANCEL_CHANGES](state) {
    state.formData = Object.assign({}, state.formData, state.project);
  },
};

const getters = {
  hasChanges: state => {
    return !_.isEqual(state.project, state.formData);
  },
};

const actions = {
  async [GET_PROJECT]({ commit }, id) {
    commit(LOADING, true);
    try {
      const { 0: data } = await http.getProjects(id);
      commit(UPDATE_PROJECT, data);
      commit(LOADING, false);
      return data.creator_id;
    } catch (error) {
      commit(LOADING, false);
      message.warn('Transactions not loaded');
      throw error;
    }
  },
  async [UPDATE_PROJECT]({ state, commit }) {
    commit(UPLOADING);
    try {
      await http.updateProject(state.formData);
      message.success('Project updated successfully');
    } catch (error) {
      message.warn('Project not updated');
    }
    commit(UPLOADING, false);
    return;
  },
  [REMOVE_PROJECT_LOGO]({ commit }) {
    commit(REMOVE_PROJECT_LOGO);
  },
  [REMOVE_PROJECT_IMAGE]({ commit }) {
    commit(REMOVE_PROJECT_IMAGE);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
